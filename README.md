# music

Kimapr's music tracks.
You can use them free of charge for any purpose, see LICENSE

# bin

Because BeepBox generates audio waves on the fly, it can be resource intensive.
If you have a weak device you can get the pre-generated audio files in `bin/`

# What is actually stored here

The tracks are made in [BeepBox](https://beepbox.co).
In BeepBox, all the music is stored in the URL after `#`.
Because of that, the link to music becomes very big (e.g 1371 characters).
So here i store `.url` files with beepbox.co links in plain text.
To play the tracks, open a `.url` file, then copy-paste its contents to
the address bar

Note that you can also play the tracks if you are offline and have
[an offline copy of BeepBox](https://beepbox.co/beepbox_offline.html).

# Why use a software license for music?

yes

# Why are the names of tracks so stupid?

Sorry I'm bad at naming
